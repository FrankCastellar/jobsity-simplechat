# Simple Chat Room (Not So Simple)

This was a fun Code Challenge as part of an interview so I can put to the test my Backend .NET Skills.

The Code contains two solutions separated in two different folders:

* ConsoleChatBot: A .net core (3.0) console application which purpose is to consume the messages from RabbitMQ queue and verify if there is information for the specified stock (using a CSV file hosted in the internet) and then call an api to post the stock information in the user's chat.

* Website: This a .netFramework (4.5.7) webapp using MVC5, which hosts a website (and also a separated API) that allows you to create users and then log into a global chat where you will be able to communicate with others users.

## Assignment

The goal of this exercise is to create a simple browser-based chat application using .NET. This application should allow several users to talk in a chatroom and also to get stock quotes from an API using a specific command.


## Mandatory Features

* (Completed) Allow registered users to log in and talk with other users in a chatroom.
* (Completed) Allow users to post messages as commands into the chatroom with the following format /stock=stock_code
* (Completed) Create a decoupled bot that will call an API using the stock_code as a parameter
(https://stooq.com/q/l/?s=aapl.us&f=sd2t2ohlcv&h&e=csv, here aapl.us is the
stock_code)
* (Completed) The bot should parse the received CSV file and then it should send a message back into the chatroom using a message broker like RabbitMQ. The message will be a stock quote using the following format: “APPL.US quote is $93.42 per share”. The post owner will be the bot.
* (Completed) Have the chat messages ordered by their timestamps and show only the last 50
messages.

## Bonus (Optional)
* (Done) Use .NET identity for users authentication
* (Done) Handle messages that are not understood or any exceptions raised within the bot.
* (Not Done) Build an installer.

## Important Dependencies
* Database: I used LocalDB along with MS SQL Express, the structure of the database is already created and it's versioned in this CodeRepository inside the App_Data folder, which database file name is -> SimpleChatDB.mdf. Therefore you don't have to run any script or configuration to setup the DB.

* IIS Express: I would suggest to host this application using IIS Express through Visual Studio, since the website has been configured to host a localhost API in port 60645 which is consumed by ConsoleBot project, therefore if you change this port you should also change the configuration of ConsoleBot application. You can configure the service endpoint for the ConsoleBot application though.

* RabbitMQ: You will need to install RabbitMQ (therefore also Earl), I'm using the default RabbitMQ values, I also assume the RabbitServer is running in the same machine as the website, however you can change those configurations for the Website and ConsoleBot as well (through config files). Both apps use the following default configs ->
    * user: guest
    * password: guest
    * virtual host: /
    * queuename: simplechat.bot.stockmessages 
Note: this one is automatically created.
    * and others ...

## Considerations
* The Console App must be executed or running at all time so it can provide the stock information to the users who requested it.
