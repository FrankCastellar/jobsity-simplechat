﻿
using SimpleChatRoom.Logic.Models;

namespace SimpleChatRoom.Logic.Services.WebSocket
{
    public interface IWebSocketChatService
    {
        void NewGlobalMessageAction(PostMessageModel message);
        void StockMessageAction(string userId, string message);
    }
}