﻿namespace SimpleChatRoom.Logic.Services.MessageBroker
{
    public class StockMessagePayload
    {
        public string UserId { get; set; }
        public string StockCode { get; set; }
    }
}
