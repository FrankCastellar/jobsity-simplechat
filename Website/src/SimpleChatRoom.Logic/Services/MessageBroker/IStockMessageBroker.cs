﻿namespace SimpleChatRoom.Logic.Services.MessageBroker
{
    public interface IStockMessageBroker
    {
        void AddNewMessageIntoQueue(StockMessagePayload message);
    }
}
