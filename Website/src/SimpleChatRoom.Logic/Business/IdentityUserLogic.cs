﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleChatRoom.Logic.Models;

namespace SimpleChatRoom.Logic.Business
{
    public partial class MainLogic
    {
        public async Task<IList<UserIdentityModel>> GetAllUsersAsync()
        {
            var elements = await UserIdentityRepository.GetAllUsersAsync();
            return Mapper.Map<IList<UserIdentityModel>>(elements);
        }

        public async Task<UserIdentityModel> FindUserByUsernameAndPasswordAsync(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username)) throw new ArgumentNullException(nameof(username));
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentNullException(nameof(password));

            var userElement = await UserIdentityRepository.FindUserByUsernameAndPasswordAsync(username, password);
            return Mapper.Map<UserIdentityModel>(userElement);
        }

        public async Task<string> CreateUserAccountAsync(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username)) throw new ArgumentNullException(nameof(username));
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentNullException(nameof(password));

            var userElement = await UserIdentityRepository.FindUserByUsernameAsync(username);
            if (userElement != null)
            {
                return "Already exists an account with that username.";
            }

            var result = await UserIdentityRepository.CreateAsync(username, password);
            
            if (result.Succeeded)
            {
                return string.Empty;
            }

            return result.Errors.FirstOrDefault() ?? string.Empty;
        }
    }
}
