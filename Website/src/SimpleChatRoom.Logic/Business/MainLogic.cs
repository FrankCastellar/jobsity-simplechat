﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SimpleChatRoom.Data.Base.Identity;
using SimpleChatRoom.Data.Base.Normal;
using SimpleChatRoom.Data.Entities;
using SimpleChatRoom.Logic.Models;
using SimpleChatRoom.Logic.Services.MessageBroker;
using SimpleChatRoom.Logic.Services.WebSocket;

namespace SimpleChatRoom.Logic.Business
{
    public partial class MainLogic
    {
        public MainLogic(IMapper mapper,
            IWebSocketChatService chatSocketService,
            IStockMessageBroker stockMessageBroker,

            AbstractUserIdentityRepository userIdentityRepository,
            AbstractPostMessageRepository postMessageRepository)
        {
            Mapper = mapper;
            ChatSocketService = chatSocketService;
            StockMessageBroker = stockMessageBroker;

            UserIdentityRepository = userIdentityRepository;
            PostMessageRepository = postMessageRepository;
        }

        public IMapper Mapper { get; set; }
        public IWebSocketChatService ChatSocketService { get; set; }
        public IStockMessageBroker StockMessageBroker { get; set; }

        public AbstractUserIdentityRepository UserIdentityRepository { get; set; }
        public AbstractPostMessageRepository PostMessageRepository { get; set; }

        public async Task SendNewMessageAsync(string message, string userId)
        {
            if(string.IsNullOrWhiteSpace(message)) throw new ArgumentNullException(message);
            if(string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(userId);

            var innerMessage = message.Trim();
            if (innerMessage.ToUpper().StartsWith("/"))
            {
                var messageParts = innerMessage.Split('=');
                if (messageParts.Length != 2) return;

                var firstPart = messageParts[0].Trim().ToUpper();
                if (firstPart != "/STOCK") return;

                var secondPart = messageParts[1];
                StockMessageBroker.AddNewMessageIntoQueue(new StockMessagePayload{ StockCode = secondPart, UserId = userId });
                return;
            }

            var postMessageElement = new PostMessage
            {
                UserId = userId,
                Message = message,
                ChangedBy = userId,
                CreatedBy = userId
            };
            PostMessageRepository.Create(postMessageElement);
            await PostMessageRepository.SaveChangesAsync();

            var postModel = Mapper.Map<PostMessageModel>(postMessageElement);
            ChatSocketService.NewGlobalMessageAction(postModel);
        }

        public async Task<IList<PostMessageModel>> GetLatestPostMessagesAsync()
        {
            var messages = (await PostMessageRepository.GetLatestMessagesAsync()).ToList();
            return Mapper.Map<IList<PostMessageModel>>(messages);
        }

        public async Task SendStockNewMessageAsync(PostMessageModel messageModel)
        {
            if (messageModel == null) throw new ArgumentNullException(nameof(messageModel));
            if (string.IsNullOrWhiteSpace(messageModel.UserId)) throw new ArgumentNullException(nameof(messageModel.UserId));
            if (string.IsNullOrWhiteSpace(messageModel.Message)) throw new ArgumentNullException(nameof(messageModel.Message));

            var userElement = await UserIdentityRepository.FindUserByUsernameAsync(messageModel.UserId);
            if (userElement == null)
            {
                throw new InvalidOperationException($"Can't post StockMessage through webSocket, the user: [{messageModel.UserId}] doesn't exist.");
            }

            ChatSocketService.StockMessageAction(messageModel.UserId, messageModel.Message);
        }
    }
}
