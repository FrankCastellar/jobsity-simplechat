﻿using AutoMapper;
using SimpleChatRoom.Common.Audit;
using SimpleChatRoom.Common.Audit.Interface;

namespace SimpleChatRoom.Logic.Extensions
{
    public static class AutoMapperExtensions
    {
        public static IMappingExpression<TSource, TDestination> ToModelIgnoreAuditProperties<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping)
            where TSource : AuditModel
            where TDestination : AuditEntity
        {
            mapping.ForMember(x => x.CreatedBy, cfg => cfg.Ignore());
            mapping.ForMember(x => x.CreatedDate, cfg => cfg.Ignore());
            mapping.ForMember(x => x.ChangedBy, cfg => cfg.Ignore());
            mapping.ForMember(x => x.ChangedDate, cfg => cfg.Ignore());

            return mapping;
        }

        public static IMappingExpression<TSource, TDestination> ToModelIgnoreIAuditProperties<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping)
            where TSource : AuditModel
            where TDestination : IAudit
        {
            mapping.ForMember(x => x.CreatedBy, cfg => cfg.Ignore());
            mapping.ForMember(x => x.CreatedDate, cfg => cfg.Ignore());
            mapping.ForMember(x => x.ChangedBy, cfg => cfg.Ignore());
            mapping.ForMember(x => x.ChangedDate, cfg => cfg.Ignore());

            return mapping;
        }
    }
}
