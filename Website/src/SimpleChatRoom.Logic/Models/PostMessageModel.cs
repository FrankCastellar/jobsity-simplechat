﻿using AutoMapper;
using SimpleChatRoom.Common.Audit;
using SimpleChatRoom.Data.Entities;
using SimpleChatRoom.Logic.Extensions;

namespace SimpleChatRoom.Logic.Models
{
    public class PostMessageModel : AuditModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Message { get; set; }
    }

    public class PostMessageModelProfile : Profile
    {
        public PostMessageModelProfile()
        {
            CreateMap<PostMessageModel, PostMessage>()
                .ToModelIgnoreAuditProperties()
                .ReverseMap();
        }
    }
}
