﻿using AutoMapper;
using SimpleChatRoom.Common.Audit;
using SimpleChatRoom.Data.Entities;
using SimpleChatRoom.Logic.Extensions;

namespace SimpleChatRoom.Logic.Models
{
    public class UserIdentityModel : AuditModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }

    public class UserIdentityModelProfile : Profile
    {
        public UserIdentityModelProfile()
        {
            CreateMap<UserIdentityModel, UserIdentity>()
                .ToModelIgnoreIAuditProperties()
                .ReverseMap();
        }
    }
}
