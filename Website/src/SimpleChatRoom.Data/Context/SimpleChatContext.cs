﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleChatRoom.Common.Audit.Interface;
using SimpleChatRoom.Data.Entities;

namespace SimpleChatRoom.Data.Context
{
    public class SimpleChatContext : IdentityDbContext<UserIdentity>
    {
        public SimpleChatContext() : base("SimpleChatDB")
        {
        }

        public virtual DbSet<PostMessage> PostMessages { get; set; }

        public override Task<int> SaveChangesAsync()
        {
            SetupDateValuesForEntries();
            return base.SaveChangesAsync();
        }

        private void SetupDateValuesForEntries()
        {
            var entries = this.ChangeTracker.Entries() ?? new List<DbEntityEntry>();
            var now = DateTime.Now;
            foreach (var entry in entries)
            {
                if (entry.Entity == null || !(entry.Entity is IAudit audit))
                    continue;

                switch (entry.State)
                {
                    case EntityState.Added:
                        audit.CreatedDate = now;
                        audit.ChangedDate = now;
                        break;
                    case EntityState.Modified:
                        audit.ChangedDate = now;
                        break;
                }
            }
        }
    }
}
