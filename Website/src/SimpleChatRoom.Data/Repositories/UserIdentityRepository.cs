﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using SimpleChatRoom.Data.Base.Identity;
using SimpleChatRoom.Data.Entities;

namespace SimpleChatRoom.Data.Repositories
{
    public class UserIdentityRepository : AbstractUserIdentityRepository
    {
        public UserIdentityRepository(UserManager<UserIdentity> manager) : base(manager)
        {
        }

        public override async Task<IList<UserIdentity>> GetAllUsersAsync()
        {
            return await Manager.Users.ToListAsync();
        }

        public override async Task<UserIdentity> FindUserByUsernameAndPasswordAsync(string username, string password)
        {
            return await Manager.FindAsync(username, password);
        }

        public override async Task<UserIdentity> FindUserByUsernameAsync(string username)
        {
            return await Manager.FindByNameAsync(username);
        }
    }
}
