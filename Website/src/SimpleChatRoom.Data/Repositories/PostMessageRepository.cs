﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using SimpleChatRoom.Data.Base.Normal;
using SimpleChatRoom.Data.Context;
using SimpleChatRoom.Data.Entities;

namespace SimpleChatRoom.Data.Repositories
{
    public class PostMessageRepository : AbstractPostMessageRepository
    {
        public PostMessageRepository(SimpleChatContext context) : base(context)
        {
        }

        public override async Task<IList<PostMessage>> GetLatestMessagesAsync()
        {
            var elements = await Context.PostMessages.OrderByDescending(x => x.CreatedDate).Take(50).ToListAsync();
            return elements.OrderBy(x => x.CreatedDate).ToList();
        }
    }
}
