﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SimpleChatRoom.Common.Audit;

namespace SimpleChatRoom.Data.Entities
{
    public class PostMessage : AuditEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Message { get; set; }
    }
}
