﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleChatRoom.Common.Audit.Interface;

namespace SimpleChatRoom.Data.Entities
{
    public class UserIdentity : IdentityUser, IAudit
    {
        public string ChangedBy { get; set; }
        public DateTime? ChangedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
