﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleChatRoom.Data.Context;
using SimpleChatRoom.Data.Entities;

namespace SimpleChatRoom.Data.Base.Normal
{
    public abstract class AbstractPostMessageRepository : AbstractRepository<PostMessage>
    {
        protected AbstractPostMessageRepository(SimpleChatContext context) : base(context)
        {
        }

        public abstract Task<IList<PostMessage>> GetLatestMessagesAsync();
    }
}
