﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using SimpleChatRoom.Data.Entities;

namespace SimpleChatRoom.Data.Base.Identity
{
    public abstract class AbstractUserIdentityRepository
    {
        protected AbstractUserIdentityRepository(UserManager<UserIdentity> manager)
        {
            Manager = manager;
        }

        public UserManager<UserIdentity> Manager { get; set; }

        public async Task<IdentityResult> CreateAsync(string username, string password)
        {
            return await Manager.CreateAsync(new UserIdentity
            {
                UserName = username
            }, password);
        }

        public abstract Task<IList<UserIdentity>> GetAllUsersAsync();
        public abstract Task<UserIdentity> FindUserByUsernameAndPasswordAsync(string username, string password);
        public abstract Task<UserIdentity> FindUserByUsernameAsync(string username);
    }
}
