﻿using System.Data.Entity;
using System.Threading.Tasks;
using SimpleChatRoom.Data.Context;

namespace SimpleChatRoom.Data.Base
{
    public abstract class AbstractRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected AbstractRepository(SimpleChatContext context)
        {
            Context = context;
        }

        public  SimpleChatContext Context { get; set; }

        public virtual TEntity Create(TEntity entity)
        {
            Context.Set<TEntity>().Attach(entity);
            var db = Context.Entry(entity);
            db.State = EntityState.Added;
            return entity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            var entry = Context.Entry(entity);
            entry.State = EntityState.Modified;
            return entity;
        }

        public virtual void Delete(TEntity entity)
        {
            var entry = Context.Entry(entity);
            entry.State = EntityState.Deleted;
        }

        public virtual async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }
    }
}
