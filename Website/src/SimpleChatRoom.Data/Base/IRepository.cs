﻿using System.Threading.Tasks;

namespace SimpleChatRoom.Data.Base
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Create(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(TEntity entity);
        Task SaveChangesAsync();
    }
}
