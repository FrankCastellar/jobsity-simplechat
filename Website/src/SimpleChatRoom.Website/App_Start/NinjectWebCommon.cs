using System;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.SignalR;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using SimpleChatRoom.Data.Base.Identity;
using SimpleChatRoom.Data.Base.Normal;
using SimpleChatRoom.Data.Context;
using SimpleChatRoom.Data.Entities;
using SimpleChatRoom.Data.Repositories;
using SimpleChatRoom.Logic.Models;
using SimpleChatRoom.Logic.Services.MessageBroker;
using SimpleChatRoom.Logic.Services.WebSocket;
using SimpleChatRoom.Website;
using SimpleChatRoom.Website.Services.MessageBroker;
using SimpleChatRoom.Website.Services.SignalR;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace SimpleChatRoom.Website
{
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                GlobalHost.DependencyResolver = new NinjectSignalRDependencyResolver(kernel);
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectHttpDependencyResolver(kernel);

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            // Services.
            kernel.Bind<IWebSocketChatService>().To<MyChatHub>().InSingletonScope();
            kernel.Bind<IStockMessageBroker>().To<RabbitMessageBroker>().InRequestScope();
            kernel.Bind<RabbitQueueHelper>().ToSelf().InRequestScope();

            // Database.
            kernel.Bind<SimpleChatContext>().ToSelf().InRequestScope();
            kernel.Bind<IUserStore<UserIdentity>>().To<UserStore<UserIdentity>>().InRequestScope().WithConstructorArgument("context", kernel.Get<SimpleChatContext>());
            kernel.Bind<UserManager<UserIdentity>>().ToSelf().InRequestScope();

            // Database Repositories.
            kernel.Bind<AbstractUserIdentityRepository>().To<UserIdentityRepository>().InRequestScope();
            kernel.Bind<AbstractPostMessageRepository>().To<PostMessageRepository>().InRequestScope();

            var mapper = InitializeAutoMapper();
            kernel.Bind<IMapper>().ToConstant(mapper.CreateMapper()).InSingletonScope();
        }

        private static MapperConfiguration InitializeAutoMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                // Internal Website
                cfg.AddProfile<UserIdentityModelProfile>();
                cfg.AddProfile<PostMessageModelProfile>();
            });

            return config;
        }
    }
}
