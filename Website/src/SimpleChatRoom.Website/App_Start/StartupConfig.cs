﻿using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using SimpleChatRoom.Data.Context;
using SimpleChatRoom.Data.Entities;

namespace SimpleChatRoom.Website
{
    public class StartupConfig
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(() => new SimpleChatContext());
            app.CreatePerOwinContext<UserManager<UserIdentity>>((options, context) =>
                new UserManager<UserIdentity>(new UserStore<UserIdentity>(context.Get<SimpleChatContext>())));

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });

            app.MapSignalR();
        }
    }
}