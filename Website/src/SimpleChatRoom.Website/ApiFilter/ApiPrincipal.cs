﻿using System;
using System.Security.Principal;
using SimpleChatRoom.Logic.Business;

namespace SimpleChatRoom.Website.ApiFilter
{
    public class ApiPrincipal : IPrincipal
    {
        public ApiPrincipal(IIdentity identity, MainLogic mainLogic)
        {
            Identity = identity;
            MainLogic = mainLogic;
        }

        public MainLogic MainLogic { get; set; }

        public IIdentity Identity { get; }

        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }
    }
}