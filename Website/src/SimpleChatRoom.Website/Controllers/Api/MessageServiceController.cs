﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using SimpleChatRoom.Logic.Business;
using SimpleChatRoom.Logic.Models;
using SimpleChatRoom.Website.ApiFilter;

namespace SimpleChatRoom.Website.Controllers.Api
{
    
    public class MessageServiceController : ApiController
    {
        public MessageServiceController(MainLogic mainLogic)
        {
            MainLogic = mainLogic;
        }

        public MainLogic MainLogic { get; set; }

        [HttpPost]
        [ActionName("PostStockMessage")]
        public async Task PostStockMessage([FromBody]PostMessageModel messageModel)
        {
            var identity = BasicAuthenticationFilter.ParseAuthorizationHeader(ActionContext);
            if (identity == null)
            {
                throw new InvalidOperationException("Invalid Basic Auth.");
            }

            if (identity.Name != "Bot" || identity.Password != "botbot")
            {
                throw new InvalidOperationException("Only Bot is allowed to perform PostStockMessage action.");
            }

            await MainLogic.SendStockNewMessageAsync(messageModel);
        }

        [HttpGet]
        [ActionName("PostStockMessageGet")]
        public async Task<string> PostStockMessage(string value)
        {
            return value;
        }
    }
}