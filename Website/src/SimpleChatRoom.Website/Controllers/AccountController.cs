﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SimpleChatRoom.Logic.Business;

namespace SimpleChatRoom.Website.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController(MainLogic mainLogic)
        {
            MainLogic = mainLogic;
        }

        public MainLogic MainLogic { get; set; }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> LoginUser(string username, string password, string returnUrl)
        {
            var userFound = await MainLogic.FindUserByUsernameAndPasswordAsync(username, password);
            if (userFound == null)
            {
                return Json(new { response = "notFound" }, JsonRequestBehavior.AllowGet);
            }

            var identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.NameIdentifier, userFound.UserName),
                    new Claim(ClaimTypes.Name, userFound.UserName)
                }, DefaultAuthenticationTypes.ApplicationCookie);

            Request.GetOwinContext().Authentication.SignIn(identity);
            return Json(new { response = "success", returnUrl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> CreateAccount(string username, string password)
        {
            var result = await MainLogic.CreateUserAccountAsync(username, password);
            return string.IsNullOrWhiteSpace(result) 
                ? Json(new { response = "success" }, JsonRequestBehavior.AllowGet)
                : Json(new { response = "error", message = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            Request.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }
    }
}