﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using SimpleChatRoom.Logic.Business;

namespace SimpleChatRoom.Website.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController(MainLogic mainLogic)
        {
            MainLogic = mainLogic;
        }

        public MainLogic MainLogic { get; set; }
        public async Task<ActionResult> Index()
        {
            var users = await MainLogic.GetAllUsersAsync();
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> SendNewMessage(string message)
        {
            var userId = User.Identity.Name;
            await MainLogic.SendNewMessageAsync(message, userId);
            return Json(new {response = "success"}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> GetLatestMessages()
        {
            var postMessages = await MainLogic.GetLatestPostMessagesAsync();
            return Json(new {response = "success", messages = postMessages}, JsonRequestBehavior.AllowGet);
        }
    }
}