﻿window.globalUtils = window.globalUtils || {};
(function (namespace) {
    const me = namespace;

    me.post = async (url, data) => {
        return await me.fetchMethod(url, data, 'POST');
    };

    me.get = async (url, data) => {
        return await me.fetchMethod(url, data, 'GET');
    };

    me.fetchMethod = async (url, data, type) => {
        const innerData = data ? JSON.stringify(data) : undefined;
        var response = undefined;
        try {
            response = await fetch(url, {
                method: type,
                cache: "no-cache",
                body: innerData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            });

            if (!response.ok) {
                console.error(response);
            }
        } catch (e) {
            console.error(e);
        }

        return response;

    };
})(window.globalUtils);