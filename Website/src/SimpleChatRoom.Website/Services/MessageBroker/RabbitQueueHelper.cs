﻿using System.Text;
using Newtonsoft.Json;
using SimpleChatRoom.Logic.Services.MessageBroker;
using SimpleChatRoom.Website.Properties;
using RabbitMQ.Client;

namespace SimpleChatRoom.Website.Services.MessageBroker
{
    public class RabbitQueueHelper
    {
        public RabbitQueueHelper()
        {
            Factory = new ConnectionFactory
            {
                HostName = Settings.Default.RabbitHostName,
                VirtualHost = Settings.Default.RabbitVirtualHost,
                Port = Settings.Default.RabbitPort,
                UserName = Settings.Default.RabbitUsername,
                Password = Settings.Default.RabbitPassword
            };
        }

        public ConnectionFactory Factory { get; set; }
        public bool AddMessageIntoQueue(StockMessagePayload messagePayload)
        {
            if (messagePayload == null) return false;

            var message = JsonConvert.SerializeObject(messagePayload);
            var body = Encoding.UTF8.GetBytes(message);
            using (var conn = Factory.CreateConnection())
            {
                using (var channel = conn.CreateModel())
                {
                    channel.QueueDeclare(Settings.Default.RabbitQueueName, Settings.Default.RabbitDurable, Settings.Default.RabbitExclusive, Settings.Default.RabbitAutoDelete, null);
                    channel.BasicPublish(string.Empty, Settings.Default.RabbitQueueName, null, body);
                }
            }

            return true;
        }
    }
}