﻿using System;
using SimpleChatRoom.Logic.Services.MessageBroker;

namespace SimpleChatRoom.Website.Services.MessageBroker
{
    public class RabbitMessageBroker : IStockMessageBroker
    {
        public RabbitMessageBroker(RabbitQueueHelper rabbitQueueHelper)
        {
            RabbitQueueHelper = rabbitQueueHelper;
        }

        public RabbitQueueHelper RabbitQueueHelper { get; set; }

        public void AddNewMessageIntoQueue(StockMessagePayload message)
        {
            if(message == null) throw new ArgumentNullException(nameof(message));
            if(string.IsNullOrWhiteSpace(message.UserId)) throw new ArgumentNullException(nameof(message.UserId));
            if (string.IsNullOrWhiteSpace(message.StockCode)) throw new ArgumentNullException(nameof(message.StockCode));

            RabbitQueueHelper.AddMessageIntoQueue(message);
        }
    }
}