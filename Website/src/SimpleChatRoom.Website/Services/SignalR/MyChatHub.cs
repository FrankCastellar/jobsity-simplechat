﻿using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using SimpleChatRoom.Logic.Models;
using SimpleChatRoom.Logic.Services.WebSocket;

namespace SimpleChatRoom.Website.Services.SignalR
{
    public class MyChatHub : Hub, IWebSocketChatService
    {
        public IHubContext MyContext => GlobalHost.ConnectionManager.GetHubContext<MyChatHub>();
        public void NewGlobalMessageAction(PostMessageModel message)
        {
            MyContext.Clients.All.newGlobalMessageAction(message);
        }

        public void StockMessageAction(string userId, string message)
        {
            var userElement = MyContext.Clients.User(userId);
            userElement?.stockMessageAction(new PostMessageModel
            {
                UserId = "Bot",
                Message = message
            });
        }
    }
}