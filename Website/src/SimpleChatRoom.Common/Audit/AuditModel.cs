﻿using System;
using SimpleChatRoom.Common.Audit.Interface;

namespace SimpleChatRoom.Common.Audit
{
    public class AuditModel : IAudit
    {
        public string ChangedBy { get; set; }
        public DateTime? ChangedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
