﻿using System;

namespace SimpleChatRoom.Common.Audit.Interface
{
    public interface IAudit
    {
        string ChangedBy { get; set; }
        DateTime? ChangedDate { get; set; }
        string CreatedBy { get; set; }
        DateTime? CreatedDate { get; set; }
    }
}
