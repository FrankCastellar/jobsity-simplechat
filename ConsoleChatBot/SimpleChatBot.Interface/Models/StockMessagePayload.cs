﻿namespace SimpleChatBot.Interface.Models
{
    public class StockMessagePayload
    {
        public string UserId { get; set; }
        public string StockCode { get; set; }
    }
}
