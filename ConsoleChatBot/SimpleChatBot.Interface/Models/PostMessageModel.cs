﻿namespace SimpleChatBot.Interface.Models
{
    public class PostMessageModel
    {
        public string UserId { get; set; }
        public string Message { get; set; }
    }
}
