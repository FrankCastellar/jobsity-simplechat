﻿namespace SimpleChatBot.Interface.Configurations
{
    public interface ISettingsProvider
    {
        RabbitQueueSettings RabbitSettings { get; set; }
        ServiceChatSettings ServiceChatSettings { get; set; }
    }
}
