﻿namespace SimpleChatBot.Interface.Configurations
{
    public class ServiceChatSettings
    {
        public string EndpointUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
