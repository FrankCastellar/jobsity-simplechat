﻿using SimpleChatBot.Interface.Queue;

namespace SimpleChatBot.Interface.Logic
{
    public interface IMainLogic
    {
        IRabbitQueueReader InstanceMessageReader();
        IRabbitQueueReader StartReadingMessages();
    }
}
