﻿using System.Threading.Tasks;
using SimpleChatBot.Interface.Models;

namespace SimpleChatBot.Interface.Service
{
    public interface ISimpleChatService
    {
        Task SendMessageToUser(PostMessageModel message);
    }
}
