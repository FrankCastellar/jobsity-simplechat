﻿using System;
using SimpleChatBot.Interface.Models;

namespace SimpleChatBot.Interface.Queue
{
    public interface IRabbitQueueReader : IDisposable
    {
        void StartReading();
        void StopReading();

        event EventHandler<StockMessagePayload> StockMessageReceivedEventHandler;
    }
}
