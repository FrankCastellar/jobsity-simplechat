﻿namespace SimpleChatBot.Interface.Queue
{
    public interface IRabbitQueueHelper
    {
        IRabbitQueueReader InstanceReader();
    }
}
