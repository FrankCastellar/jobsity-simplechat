﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SimpleChatBot.Interface.Configurations;
using SimpleChatBot.Interface.Models;
using SimpleChatBot.Interface.Service;

namespace SimpleChatBot.ConsoleApp.Service
{
    public class SimpleChatService : ISimpleChatService
    {
        private static HttpClient _client;
        public SimpleChatService(ISettingsProvider settingsProvider)
        {
            Settings = settingsProvider.ServiceChatSettings;
        }

        private static HttpClient Client => _client ?? (_client = new HttpClient());

        public ServiceChatSettings Settings { get; set; }
        public async Task SendMessageToUser(PostMessageModel message)
        {
            AddSecurityHeaders();
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var data = JsonConvert.SerializeObject(message);

            var content = new StringContent(data, Encoding.UTF8, "application/json");
            var serviceUrl = Settings.EndpointUrl;
            await Client.PostAsync(serviceUrl, content);
        }

        private void AddSecurityHeaders()
        {

            var authData = $"{Settings.Username}:{Settings.Password}";

            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
        }
    }
}
