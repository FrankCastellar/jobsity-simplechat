﻿using Microsoft.Extensions.Options;
using SimpleChatBot.Interface.Configurations;

namespace SimpleChatBot.ConsoleApp.Configurations
{
    public class SettingProvider : ISettingsProvider
    {
        public SettingProvider(IOptions<AppSettings> options)
        {
            RabbitSettings = options.Value.MessageQueue;
            ServiceChatSettings = options.Value.ServiceChat;
        }

        public RabbitQueueSettings RabbitSettings { get; set; }
        public ServiceChatSettings ServiceChatSettings { get; set; }
    }
}
