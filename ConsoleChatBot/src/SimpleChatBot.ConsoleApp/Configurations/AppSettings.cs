﻿using SimpleChatBot.Interface.Configurations;

namespace SimpleChatBot.ConsoleApp.Configurations
{
    public class AppSettings
    {
        public const string SettingsIdentifier = "AppSettings";

        public RabbitQueueSettings MessageQueue { get; set; }
        public ServiceChatSettings ServiceChat { get; set; }
    }
}
