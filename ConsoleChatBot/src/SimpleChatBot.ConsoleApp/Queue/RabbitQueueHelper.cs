﻿using RabbitMQ.Client;
using SimpleChatBot.Interface.Configurations;
using SimpleChatBot.Interface.Queue;

namespace SimpleChatBot.ConsoleApp.Queue
{
    public class RabbitQueueHelper : IRabbitQueueHelper
    {
        public RabbitQueueHelper(ISettingsProvider settingsProvider)
        {
            Settings = settingsProvider.RabbitSettings;
            Factory = new ConnectionFactory
            {
                HostName = Settings.HostName,
                VirtualHost = Settings.VirtualHost,
                Port = Settings.Port,
                UserName = Settings.Username,
                Password = Settings.Password
            };
        }

        public RabbitQueueSettings Settings { get; set; }
        public ConnectionFactory Factory { get; set; }
        public IRabbitQueueReader InstanceReader()
        {
            return new RabbitQueueReader(Factory, Settings);
        }
    }
}
