﻿using System;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SimpleChatBot.Interface.Configurations;
using SimpleChatBot.Interface.Models;
using SimpleChatBot.Interface.Queue;

namespace SimpleChatBot.ConsoleApp.Queue
{
    public class RabbitQueueReader : IRabbitQueueReader
    {
        private IConnection _connection;
        private IModel _channel;
        private bool _disposed;

        public RabbitQueueReader(ConnectionFactory factory, RabbitQueueSettings settings)
        {
            Factory = factory;
            Settings = settings;
        }

        public event EventHandler<StockMessagePayload> StockMessageReceivedEventHandler;

        public RabbitQueueSettings Settings { get; set; }
        public ConnectionFactory Factory { get; set; }

        public void StartReading()
        {
            _connection = Factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(Settings.QueueName, Settings.Durable, Settings.Exclusive, Settings.AutoDelete, null);
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += ConsumerOnReceived;
            _channel.BasicConsume(Settings.QueueName, true, consumer);
        }

        public void StopReading()
        {
            if (_disposed) return;

            try
            {
                _channel?.Dispose();
            }
            catch (Exception)
            {
                // TODO: log error.
            }
            finally
            {
                _channel = null;
            }

            try
            {
                _connection?.Dispose();
            }
            catch (Exception)
            {
                // TODO: log error.
            }
            finally
            {
                _connection = null;
            }

            _disposed = true;
        }

        public void Dispose()
        {
            StopReading();
        }

        private void ConsumerOnReceived(object sender, BasicDeliverEventArgs e)
        {
            var textMessage = Encoding.UTF8.GetString(e.Body);
            var emailMessage = JsonConvert.DeserializeObject<StockMessagePayload>(textMessage);
            StockMessageReceivedEventHandler?.Invoke(sender, emailMessage);
        }
    }
}
