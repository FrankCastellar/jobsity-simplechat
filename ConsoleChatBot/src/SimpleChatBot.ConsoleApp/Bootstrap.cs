﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SimpleChatBot.ConsoleApp.Configurations;
using SimpleChatBot.ConsoleApp.Logic;
using SimpleChatBot.ConsoleApp.Queue;
using SimpleChatBot.ConsoleApp.Service;
using SimpleChatBot.Interface.Configurations;
using SimpleChatBot.Interface.Logic;
using SimpleChatBot.Interface.Queue;
using SimpleChatBot.Interface.Service;

namespace SimpleChatBot.ConsoleApp
{
    public class Bootstrap
    {
        public static IMainLogic GetReaderAppInstance()
        {
            var serviceCollection = ComposeServiceCollection();
            var serviceProvider = serviceCollection.BuildServiceProvider();

            return serviceProvider.GetService<IMainLogic>();
        }

        private static IServiceCollection ComposeServiceCollection()
        {
            var configuration = ComposeConfigurationRoot();
            IServiceCollection serviceCollection = new ServiceCollection();

            // Setting up app-settings.
            var settingsSection = configuration.GetSection(AppSettings.SettingsIdentifier);
            serviceCollection.Configure<AppSettings>(settingsSection);

            // Configuring Logging.
            serviceCollection.AddLogging(configure => configure.AddConsole())
                .Configure<LoggerFilterOptions>(options => options.MinLevel = LogLevel.Information)
                .AddTransient<MainLogic>();

            // Configuring Dependencies.
            serviceCollection.AddTransient<IRabbitQueueHelper, RabbitQueueHelper>();
            serviceCollection.AddSingleton<ISettingsProvider, SettingProvider>();
            serviceCollection.AddSingleton<IMainLogic, MainLogic>();
            serviceCollection.AddSingleton<ISimpleChatService, SimpleChatService>();

            return serviceCollection;
        }

        private static IConfigurationRoot ComposeConfigurationRoot()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            return configuration;
        }
    }
}
