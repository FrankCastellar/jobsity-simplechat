﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SimpleChatBot.ConsoleApp.Service;
using SimpleChatBot.Interface.Logic;
using SimpleChatBot.Interface.Models;
using SimpleChatBot.Interface.Queue;
using SimpleChatBot.Interface.Service;

namespace SimpleChatBot.ConsoleApp.Logic
{
    public class MainLogic : IMainLogic
    {
        public MainLogic(IRabbitQueueHelper messageQueueHelper, ISimpleChatService chatService, ILogger<MainLogic> logger)
        {
            MessageQueueHelper = messageQueueHelper;
            Logger = logger;
            ChatService = chatService;
        }

        public IRabbitQueueHelper MessageQueueHelper { get; set; }
        public ISimpleChatService ChatService { get; set; }
        public ILogger<MainLogic> Logger { get; set; }

        public IRabbitQueueReader InstanceMessageReader()
        {
            try
            {
                var instance = MessageQueueHelper.InstanceReader();
                instance.StockMessageReceivedEventHandler += InstanceOnStockMessageReceivedEventHandler;
                return instance;
            }
            catch (Exception exception)
            {
                Logger.LogError("An error occurred at Instancing Reader.", exception);
                throw;
            }
        }

        public IRabbitQueueReader StartReadingMessages()
        {
            try
            {
                var instance = MessageQueueHelper.InstanceReader();
                instance.StockMessageReceivedEventHandler += InstanceOnStockMessageReceivedEventHandler;
                instance.StartReading();
                return instance;
            }
            catch (Exception exception)
            {
                Logger.LogError("An error has occurred at Composing Reader and Starting to read messages.", exception);
                throw;
            }
        }

        private void InstanceOnStockMessageReceivedEventHandler(object sender, StockMessagePayload e)
        {
            Logger.LogInformation($"\nStock Message Received: {JsonConvert.SerializeObject(e)}.");

            var stockCode = e.StockCode;
            if (string.IsNullOrWhiteSpace(stockCode))
            {
                Logger.LogError("StockCode can't be null.");
                return;
            }
            stockCode = stockCode.Trim();

            if (string.IsNullOrWhiteSpace(e.UserId))
            {
                Logger.LogError("UserId can't be null.");
                return;
            }

            try
            {
                var composingUrl = $"https://stooq.com/q/l/?s={stockCode.ToLower()}&f=sd2t2ohlcv&h&e=csv";
                var webRequest = WebRequest.Create(composingUrl);

                var message = string.Empty;
                using (var response = webRequest.GetResponse())
                {
                    using (var content = response.GetResponseStream())
                    {
                        if(content == null) throw new InvalidOperationException("Error at getting response Stream");
                        using (var reader = new StreamReader(content))
                        {
                            if (!reader.EndOfStream)
                            {
                                // ignore first line (headers).
                                reader.ReadLine();
                            }
                            
                            while (!reader.EndOfStream)
                            {
                                var line = reader.ReadLine();
                                if( string.IsNullOrWhiteSpace(line))
                                {
                                    continue;
                                }

                                var values = line.Split(',');
                                if (values.Length < 4)
                                {
                                    message = $"Invalid Stock Source, stock searched -> [{stockCode}].";
                                    break;
                                }

                                var symbol = values[0];
                                var quoteValue = values[3];

                                if (string.IsNullOrWhiteSpace(quoteValue) || !decimal.TryParse(quoteValue.Trim(), out _)) continue;

                                message = $"{symbol} quote is ${quoteValue.Trim()} per share.";
                                break;
                            }
                        }
                    }
                }

                if (string.IsNullOrWhiteSpace(message))
                {
                    message = $"Stock -> [{e.StockCode.Trim()}] not found.";
                }

                ChatService.SendMessageToUser(new PostMessageModel
                {
                    UserId = e.UserId,
                    Message = message
                });
            }
            catch (Exception exception)
            {
                Logger.LogError($"An error occured at processing StockMessagePayload with UserId: [{e.UserId}] and StockCode: [{e.StockCode}].", exception);
            }
        }
    }
}
