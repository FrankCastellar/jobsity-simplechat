﻿using System;
using Newtonsoft.Json;

namespace SimpleChatBot.ConsoleApp
{
    public class Program
    {
        static void Main()
        {
            Console.WriteLine("Starting Service Reader!");
            var appInstance = Bootstrap.GetReaderAppInstance();
            using (var reader = appInstance.InstanceMessageReader())
            {
                try
                {
                    reader.StartReading();
                    Console.WriteLine("Service Reader Started!");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"An error occurred at Initiating Message Reader. Error -> {e}");
                }

                Console.WriteLine("Press any key to stop application.");
                Console.ReadLine();
            }

            Console.WriteLine("Service Reader Ended!");
        }
    }
}
